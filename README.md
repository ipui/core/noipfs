# NoIpfs

no ipfs node placeholder

## props
| name | type | default | description |
| ---- | ---- | ------- | ----------- |
| `links` | `object` | `{ homepage: '#', issues: '#' }` | links to be render on placeholder. |
