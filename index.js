import React from 'react'

import {
  FiHeart,
  FiCode
} from 'react-icons/fi'

import {
  AiOutlineBug
} from 'react-icons/ai'

import './NoIpfs.sass'

const NoIpfs = props => {
  const { links = { homepage: '#', issues: '#' } } = props

  return (
    <section className="no-ipfs">
      <p>
      an <a href="https://ipfs.io" target="_blank" rel="noopener noreferrer">ipfs</a> node is required to use this dapp.
      </p>
      <p>
        made with <FiHeart className="red" />
      </p>
      <ul>
        <li>
          <a href={ links.homepage } target="_blank" rel="noopener noreferrer">
            <FiCode /> source code
          </a>
        </li>
        <li>
          <a href={ links.issues } target="_blank" rel="noopener noreferrer">
            <AiOutlineBug /> issues
          </a>
        </li>
      </ul>
    </section>
  )
}

export default NoIpfs
